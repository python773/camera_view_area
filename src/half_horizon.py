import math
from dataclasses import dataclass

import geopandas as gpd
from shapely import affinity
from shapely.geometry import Point

from src.enums import Side
from src.gdf_validation import GDFValidator


def point_offsetter(
    main_point: Point, azimuth: float, distance: float, side: Side = Side.FRONT
) -> Point:
    """Offset point geometry by: distance, azimuth and side

    Parameters
    ----------
    main_point : Point
        main reference point
    azimuth : float
        azimuth
    offset : float
        line offset value in [m]
    side : Side
        FRONT/BACK side of azimuth direction

    Returns
    -------
    moved_point : Point
        offset Point object
    """
    factor = 1 if side.value == "front" else -1

    moved_point = Point(
        round(main_point.x + math.sin(azimuth) * distance * factor, 3),
        round(main_point.y + math.cos(azimuth) * distance * factor, 3),
    )
    return moved_point


@dataclass
class HalfHorizon:
    """Return GDF with geometry of Half horizon
    - intersection of circle optionally with offset from main point

    Parameters
    ----------
    in_gdf : gpd.GeoDataFrame
        input gpd with point geometry field, azimuth column
    radius : float
        max value of range of the field view
    offset_circle : float
        optional offset of radius [m]
    side : Side
        FRONT/BACK side of azimuth direction

    Returns
    -------
    gdf : gpd.GeoDataFrame
        Updated GeoDataFrame with polygon geometry - half horizon
    """

    valid_gdf: GDFValidator
    radius: float = 75
    offset_circle: float = 20
    side: Side = Side.FRONT

    def __post_init__(self):
        self.gdf = gpd.GeoDataFrame(self.valid_gdf.gpd.copy())
        self.gdf = self.half_horizon()

    def half_horizon(self) -> gpd.GeoDataFrame:
        for idx, row in self.gdf.iterrows():
            main_point: Point = row.geometry
            azimuth: float = row[self.valid_gdf.azimuth_col]

            moved_main: Point = point_offsetter(
                main_point=main_point,
                azimuth=math.radians(azimuth),
                distance=2 * self.radius,
                side=self.side,
            )

            offset_point: Point = point_offsetter(
                main_point=main_point,
                azimuth=math.radians(azimuth),
                distance=self.offset_circle,
                side=self.side,
            )

            # Square moved polygon
            square = moved_main.buffer(self.radius * 2, cap_style="square")

            # Rotated square moved polygon
            rotated_square = affinity.rotate(square, 360 - azimuth)

            # Circle offset buffer
            circle = offset_point.buffer(self.radius / 2)

            # Intersection of Rotated square moved polygon and Circle with offset
            geometry = circle.intersection(rotated_square)

            # Assign new geometry
            self.gdf.loc[idx, "geometry"] = geometry

        return self.gdf

    @property  # returner gpd
    def gpd(self) -> gpd.GeoDataFrame:
        return self.gdf
