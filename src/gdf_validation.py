import math
from dataclasses import dataclass

import geopandas as gpd

from src.enums import AnglesUnit


@dataclass
class GDFValidator:
    """GDF Validate conditions:
    - exists geometry column
    - point geometry of gdf
    - exists azimuth column
    - optional recalculate azimuth in degrees

    Parameters
    ----------
    in_gdf : gpd.GeoDataFrame
        input gpd with point geometry field, azimuth column
    azimuth_col : str
        column in gdf with azimuth value
    azimuth_unit : AnglesUnit
        units of angle (azimuth)
    geom_col : str
        column in gdf with point geometry

    Returns
    -------
    gdf_copy : Point
        offset Point object
    """

    in_gdf: gpd.GeoDataFrame
    azimuth_col: str = "yaw"
    azimuth_unit: AnglesUnit = AnglesUnit.DEGREES
    geom_col: str = "geometry"

    @staticmethod
    def check_column(col: str, in_gdf: gpd.GeoDataFrame) -> None:
        if not col in in_gdf.columns:
            raise AttributeError(f"No column named {col} in input gdf")

    def __post_init__(self):
        # * Validation rules
        # Columns exists
        [
            GDFValidator.check_column(col, self.in_gdf)
            for col in [self.azimuth_col, self.geom_col]
        ]

        # Geometry validation
        if not self.in_gdf.geom_type.nunique() == 1:
            raise ValueError("Not unique geometry types in input gdf")
        if not self.in_gdf.geom_type.unique()[0] == "Point":
            raise ValueError(
                f"Invalid geometry type {self.gdf.geom_type.unique()[0]} in input gdf"
            )

        #! copy of input gdf!
        self.gdf = gpd.GeoDataFrame(self.in_gdf.copy())

        # * Convert radians if necessery
        if self.azimuth_unit.value == "radians":
            self.gdf[self.azimuth_col] = self.gdf.apply(
                lambda row: math.degrees(row[self.azimuth_col]), axis=1
            )

    @property  # returner gpd
    def gpd(self) -> gpd.GeoDataFrame:
        return self.gdf
