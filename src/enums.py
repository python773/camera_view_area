from enum import StrEnum, auto


class AnglesUnit(StrEnum):
    DEGREES = auto()
    RADIANS = auto()


class Side(StrEnum):
    FRONT = auto()
    BACK = auto()
