import geopandas as gpd

from src.enums import AnglesUnit, Side
from src.gdf_validation import GDFValidator
from src.half_horizon import HalfHorizon


def main():
    input_data = gpd.read_file(r"_test_data\cam0.gpkg")
    valid_gdf = GDFValidator(
        in_gdf=input_data,
        azimuth_col="yaw",
        azimuth_unit=AnglesUnit.DEGREES,
        geom_col="geometry",
    )

    half_horizon = HalfHorizon(
        valid_gdf=valid_gdf, radius=75, offset_circle=20, side=Side.FRONT
    )

    half_horizon.gpd.to_file(
        r"_test_data\test.gpkg",
        layer="horizon",
        crs="EPSG:2180",
    )


if __name__ == "__main__":
    main()
